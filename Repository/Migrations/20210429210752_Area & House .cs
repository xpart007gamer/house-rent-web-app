﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Repository.Migrations
{
    public partial class AreaHouse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<int>(nullable: true),
                    LastUpdatedBy = table.Column<int>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdatedDate = table.Column<DateTime>(nullable: true),
                    EffectiveFrom = table.Column<DateTime>(nullable: true),
                    EffectiveTo = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: true),
                    ShortValue = table.Column<string>(nullable: true),
                    AreaName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Houses",
                columns: table => new
                {
                    HouseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<int>(nullable: true),
                    LastUpdatedBy = table.Column<int>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdatedDate = table.Column<DateTime>(nullable: true),
                    EffectiveFrom = table.Column<DateTime>(nullable: true),
                    EffectiveTo = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Guid = table.Column<Guid>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: true),
                    ShortValue = table.Column<string>(nullable: true),
                    AreaId = table.Column<int>(nullable: false),
                    SectorNo = table.Column<string>(nullable: true),
                    RoadNo = table.Column<string>(nullable: true),
                    HouseNo = table.Column<string>(nullable: true),
                    ShortAddress = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false),
                    NoOfBedRooms = table.Column<int>(nullable: true),
                    NoOfBathrooms = table.Column<int>(nullable: true),
                    NoOfBelcony = table.Column<int>(nullable: true),
                    FloorNo = table.Column<int>(nullable: true),
                    SizeInSquareFeet = table.Column<decimal>(nullable: true),
                    GenderId = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: true),
                    IsPriceNegotiable = table.Column<bool>(nullable: true),
                    IsPriceWithElectricityBill = table.Column<bool>(nullable: true),
                    IsPriceWithGasBill = table.Column<bool>(nullable: true),
                    IsLiftAvailable = table.Column<bool>(nullable: true),
                    IsGarageAvailable = table.Column<bool>(nullable: true),
                    IsCCTVAvailable = table.Column<bool>(nullable: true),
                    IsGasAvailable = table.Column<bool>(nullable: true),
                    IsWifiAvailable = table.Column<bool>(nullable: true),
                    IsFireAlarmAvailable = table.Column<bool>(nullable: true),
                    LongDescription = table.Column<string>(nullable: true),
                    PictureUrl = table.Column<string>(nullable: true),
                    MapUrl = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    MobileNumber1 = table.Column<string>(nullable: true),
                    MobileNumber2 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Houses", x => x.HouseId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Areas");

            migrationBuilder.DropTable(
                name: "Houses");
        }
    }
}
