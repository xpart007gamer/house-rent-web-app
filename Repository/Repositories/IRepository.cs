﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public interface IRepository<T> where T:class
    {
        T Find(int id);
        Task<T> GetByIdAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        IQueryable<T> AsQueryable();
        IQueryable<T> AsQueryableWithNoTracking();
        IQueryable<T> AsQueryableWithoutDeleteFilter();
        IEnumerable<T> GetBetween(int start, int size);
        IQueryable<T> Where(Expression<Func<T, bool>> predicate);
      
   
        void Add(T entity);
        void Update(T entity);

        void Delete(T entity);

        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        T FirstOrDefault();

        bool Any();
        bool Any(Expression<Func<T, bool>> predicate);
    }
}
