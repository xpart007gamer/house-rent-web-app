﻿using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly ProjectDbContext db;
        private readonly DbSet<T> dbSet;

        public BaseRepository(ProjectDbContext db)
        {
            this.db = db;
            this.dbSet = this.db.Set<T>();
        }
        public void Add(T entity)
        {
            this.dbSet.Add(entity);
        }

        public T Find(int id)
        {
            return dbSet.Find(id);
        }
        public IQueryable<T> AsQueryable()
        {
            return dbSet.Where(w => w.IsDeleted != true);
        }
        public IQueryable<T> AsQueryableWithNoTracking()
        {
            return dbSet.AsNoTracking().Where(w => w.IsDeleted != true);
        }
        public IQueryable<T> AsQueryableWithoutDeleteFilter()
        {
            return dbSet.AsQueryable();
        }
        public IEnumerable<T> GetBetween(int start, int size)
        {
            return dbSet.Where(w => w.IsDeleted != true).Skip(start).Take(size);
        }
        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return dbSet.Where(w => w.IsDeleted != true).Where(predicate);
        }
        public void Delete(T entity)
        {
            this.dbSet.Remove(entity);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return this.dbSet.Where(w => w.IsDeleted != true).SingleOrDefault(predicate);
        }

        public T FirstOrDefault()
        {
            return this.dbSet.Where(w => w.IsDeleted != true).FirstOrDefault();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await this.dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await this.dbSet.FindAsync(id);
        }

        public void Update(T entity)
        {
            this.dbSet.Update(entity);
            this.db.Entry(entity).State = EntityState.Modified;
        }


        public bool Any()
        {
            return dbSet.Where(w => w.IsDeleted != true).Any();
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return dbSet.Where(w => w.IsDeleted != true).Any(predicate);
        }
    }
}
