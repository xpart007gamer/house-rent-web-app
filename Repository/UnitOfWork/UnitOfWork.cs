﻿using Entities.Models;
using Repository.Context;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Repository.UnitOfWork
{
    public class UnitOfWork : IUnitofWork
    {


        private readonly ProjectDbContext Db;

        private BaseRepository<District> districtRepo;
        private BaseRepository<Area> areaRepo;
        private BaseRepository<House> houseRepo;

        public IRepository<House> HouseRepo
        {
            get
            {
                if (houseRepo == null)
                {
                    houseRepo = new BaseRepository<House>(Db);
                }
                return houseRepo;
            }
        }
        public IRepository<Area> AreaRepo
        {
            get
            {
                if (areaRepo == null)
                {
                    areaRepo = new BaseRepository<Area>(Db);
                }
                return areaRepo;
            }
        }

        public IRepository<District> DistrictRepo
        {
            get
            {
                if (districtRepo == null)
                {
                    districtRepo = new BaseRepository<District>(Db);
                }
                return districtRepo;
            }
        }

        public UnitOfWork(ProjectDbContext Db)
        {
            this.Db = Db;
        }
        public async Task SaveChangesAsync()
        {
          await  this.Db.SaveChangesAsync();
        }

        public void Dispose()
        {
           this.Db.Dispose();
        }

       
    }
}
