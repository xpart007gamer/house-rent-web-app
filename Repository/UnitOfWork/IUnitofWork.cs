﻿using Entities.Models;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UnitOfWork
{
    public interface IUnitofWork:IDisposable
    {
        IRepository<District> DistrictRepo { get; }
        IRepository<Area> AreaRepo { get; }
        IRepository<House> HouseRepo { get; }
        Task SaveChangesAsync();
     
    }
}
