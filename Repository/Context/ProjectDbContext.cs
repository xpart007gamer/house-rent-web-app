﻿using Entities.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Context
{
    public class ProjectDbContext : IdentityDbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
           : base(options)
        {
        }

        public DbSet<District> Districts { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<House> Houses { get; set; }

    }
}
