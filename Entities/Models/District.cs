﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Models
{
    public class District : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string DistrictName { get; set; }
    }
}
