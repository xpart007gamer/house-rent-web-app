﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Models
{
    public class House : BaseEntity
    {
        [Key]
        public int HouseId { get; set; }

        public int AreaId { get; set; }
        public string SectorNo { get; set; }
        public string RoadNo { get; set; }
        public string HouseNo { get; set; }
        public string ShortAddress { get; set; }
        public int CategoryId { get; set; }
        public int? NoOfBedRooms { get; set; }
        public int? NoOfBathrooms { get; set; }
        public int? NoOfBelcony { get; set; }
        public int? FloorNo { get; set; }

        public decimal? SizeInSquareFeet { get; set; }

        public int? GenderId { get; set; }
        public decimal? Price { get; set; }
        public bool? IsPriceNegotiable { get; set; }
        public bool? IsPriceWithElectricityBill { get; set; }
        public bool? IsPriceWithGasBill { get; set; }
        public bool? IsLiftAvailable { get; set; }
        public bool? IsGarageAvailable { get; set; }
        public bool? IsCCTVAvailable { get; set; }
        public bool? IsGasAvailable { get; set; }
        public bool? IsWifiAvailable { get; set; }
        public bool? IsFireAlarmAvailable { get; set; }
        public string LongDescription { get; set; }
        public string PictureUrl { get; set; }
        public string MapUrl { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNumber1 { get; set; }
        public string MobileNumber2 { get; set; }
    }
}
