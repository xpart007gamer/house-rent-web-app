﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    public class BaseEntity
    {
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid Guid { get; set; }
        public bool? IsDefault { get; set; }
        public string ShortValue { get; set; }
    }
}
